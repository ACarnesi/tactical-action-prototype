﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChargeEnemy : Enemy {

	public float activationDist;
	public float minChargeDist;
	public float maxChargeDist;
	public float attackSpeed;
	public float chargeSpeed;
	public LayerMask enemyMask, wallMask; 

	private bool isActive;
	private float damage;
	private List<Node> path = new List<Node> ();
	private PlayerController player;
	private Animator weaponAnim;

	//Activates or deactivates the enemy. Finds a path if activating
	private bool IsActive
	{
		set
		{
			isActive = value;
			if(value == true)
			{
				StartCoroutine ("CircleTarget");
			}
		}
	}

	new void Start () 
	{
		base.Start ();
		damage = 5.0f;
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
		weaponAnim = GetComponentInChildren<Animator>();
	}

	void Update()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    if(!isActive)
		    {
			    Vector3 playerDir = (player.transform.position - transform.position).normalized;
			    RaycastHit hitInfo;
			    if(Physics.Raycast(transform.position, playerDir, out hitInfo, activationDist, enemyMask))
			    {
				    if(hitInfo.collider.tag == "Player")
					    IsActive = true;
			    }
		    } 
    //		else if(!isFollowingPath && !isDisabled)
    //		{
    //			RequestPath (transform.position, player.CurrentNode.worldPosition);
    //		}
        }
	}

	public override void OnPathFound(List<Node> newPath, bool pathSuccessful){
		if (!isDisabled) {
			if(pathSuccessful){
				//If the player is already following a path, continue its movement to its nextNode and append the new path to the end of that. 
				if(!isFollowingPath)
				{
					isFollowingPath = true;
					weaponAnim.SetBool("FollowingPath", true);
					path = newPath;
					StartCoroutine("PursueTarget");
				}
				else
				{
					newPath.Insert(0, CurrentWaypoint);
					path = newPath;
				}
			}
		}
	}

	IEnumerator CircleTarget()
	{
        if (!GUIManager.Instance.GamePaused)
        {
            if (!isDisabled)
            {
                isFollowingPath = true;
                weaponAnim.SetBool("FollowingPath", true);
                RaycastHit hitInfo;
                RaycastHit hitInfoNext;
                Node nextNode;
                Vector3 currentDirection;
                //Start with a random direction in the general direction of the player
                int ranNum = Random.Range(0, 2);
                if (ranNum == 0)
                    currentDirection = GetTurnDirection(0, 1);
                else
                    currentDirection = GetTurnDirection(1, 0);

                Vector3 turnDirection = Vector3.zero;
                Vector3 playerDir = (player.transform.position - transform.position).normalized;
                while (Physics.Raycast(transform.position, playerDir, out hitInfo, 100, enemyMask))
                {
                    if (!GUIManager.Instance.GamePaused)
                    {
                        if (!IsDisabled)
                        {

                            if (hitInfo.collider.tag == "Player")
                            {
                                //Are you far enough away to charge?
                                if (hitInfo.distance >= minChargeDist)
                                {
                                    //Are you close enough to charge?
                                    if (hitInfo.distance <= maxChargeDist)
                                    {
                                        if (ChargeCheck())
                                        {
                                            StartCoroutine("Charge");
                                            yield break;
                                        }
                                    }

                                    nextNode = NodeManager.Instance.grid[currentNode.gridX + (int)currentDirection.x, currentNode.gridY + (int)currentDirection.z];
                                    turnDirection = GetTurnDirection((int)currentDirection.x, (int)currentDirection.z);
                                    if (Physics.Raycast(transform.position, turnDirection, out hitInfo, 100, wallMask) && Physics.Raycast((transform.position + currentDirection), turnDirection, out hitInfoNext, 100, wallMask))
                                    {
                                        if (hitInfoNext.distance < hitInfo.distance)
                                        {
                                            currentDirection = turnDirection;
                                            nextNode = NodeManager.Instance.grid[currentNode.gridX + (int)currentDirection.x, currentNode.gridY + (int)currentDirection.z];
                                        }
                                        else if (hitInfoNext.distance > hitInfo.distance)
                                        {
                                            nextNode = NodeManager.Instance.grid[currentNode.gridX + (int)currentDirection.x, currentNode.gridY + (int)currentDirection.z];
                                            currentDirection = turnDirection;
                                        }
                                        else
                                            nextNode = NodeManager.Instance.grid[currentNode.gridX + (int)currentDirection.x, currentNode.gridY + (int)currentDirection.z];
                                    }
                                    if (nextNode.walkable && !nextNode.containsTrap && nextNode.creature == null)
                                    {
                                        nextNode.creature = this;
                                        transform.eulerAngles = TurnTowards(nextNode.worldPosition);
                                        while (transform.position != nextNode.worldPosition)
                                        {
                                            if (!GUIManager.Instance.GamePaused)
                                            {
                                                if (!IsDisabled)
                                                {
                                                    transform.position = Vector3.MoveTowards(transform.position, nextNode.worldPosition, speed * Time.deltaTime);
                                                    yield return null;
                                                }
                                                else
                                                {
                                                    nextNode.creature = null;
                                                    yield break;
                                                }
                                            }
                                            else
                                                yield return null;
                                        }
                                        currentNode.creature = null;
                                        currentNode = nextNode;
                                    }
                                    else
                                    {
                                        currentDirection = turnDirection;
                                        yield return null;
                                    }
                                    //					}
                                }
                                //Engage enemy in melee combat
                                else
                                {
                                    isFollowingPath = false;
                                    weaponAnim.SetBool("FollowingPath", false);
                                    RequestPath(transform.position, player.CurrentNode.worldPosition);
                                    yield break;
                                }
                            }

                            //Find the player
                            else
                            {
                                isFollowingPath = false;
                                weaponAnim.SetBool("FollowingPath", false);
                                RequestPath(transform.position, player.CurrentNode.worldPosition);
                                yield break;
                            }
                            playerDir = (player.transform.position - transform.position).normalized;
                        }
                        else
                        {
                            yield break;
                        }
                    }
                    else
                        yield return null;
                }
            }
        }
        else
            yield return null;
	}

	IEnumerator PursueTarget()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    //Move through from waypoint to waypoint until you reach the player, then attack
		    for (int i = 0; i < path.Count; i++) 
		    {
			    Node nextNode;
			    nextNode = path [i];
			    transform.eulerAngles = TurnTowards(nextNode.worldPosition);
			    //Manually navigate until you reach the next node
			    while (transform.position != nextNode.worldPosition && isDisabled == false) 
			    {
                    if (!GUIManager.Instance.GamePaused)
                    {
                        //Do you have line of sight on the player (ignores other enemies)
                        Vector3 playerDir = (player.transform.position - transform.position).normalized;
                        RaycastHit hitInfo;
                        if (Physics.Raycast(transform.position, playerDir, out hitInfo, 100, enemyMask))
                        {
                            if (hitInfo.collider.tag == "Player")
                            {
                                //Are you far enough away to charge?
                                if (hitInfo.distance >= minChargeDist)
                                {
                                    StartCoroutine("CircleTarget");
                                    yield break;
                                }
                            }
                        }

                        currentWaypoint = currentNode;
                        List<Node> neighbours = NodeManager.Instance.GetNeighbours(currentNode);
                        float bestDistance = Mathf.Abs(Vector3.Distance(currentNode.worldPosition, nextNode.worldPosition));
                        //Find the next closest node to the waypoint. 
                        foreach (Node n in neighbours)
                        {
                            //If one of the nodes contains the target, stop processing pathing and Attack.
                            if (n.CreatureOnNode == player)
                            {
                                isFollowingPath = false;
                                weaponAnim.SetBool("FollowingPath", false);
                                StartCoroutine("AttackPlayer");
                                yield break;
                            }
                            else if (n.CreatureOnNode != null || n.walkable == false)
                                continue;
                            else if (Mathf.Abs(Vector3.Distance(n.worldPosition, nextNode.worldPosition)) < bestDistance)
                            {
                                currentWaypoint = n;
                                bestDistance = Mathf.Abs(Vector3.Distance(n.worldPosition, nextNode.worldPosition));
                            }
                        }

                        //If the closest walkable node was not your own, move to that node. 
                        if (currentWaypoint != currentNode)
                        {
                            currentWaypoint.CreatureOnNode = this;

                            transform.eulerAngles = TurnTowards(CurrentWaypoint.worldPosition);
                            while (transform.position != currentWaypoint.worldPosition)
                            {
                                if (!GUIManager.Instance.GamePaused)
                                {
                                    if (!isDisabled)
                                    {
                                        transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.worldPosition, speed * Time.deltaTime);
                                        yield return null;
                                    }
                                    else
                                    {
                                        yield break;
                                    }
                                }
                                else
                                    yield return null;
                            }
                            currentNode.CreatureOnNode = null;
                            currentNode = currentWaypoint;
                        }
                        else
                            yield return null;

                        //If the player has changed positions, correct path
                        if (player.CurrentNode != targetNode)
                        {
                            RequestPath(transform.position, player.CurrentNode.worldPosition);
                            isFollowingPath = false;
                            weaponAnim.SetBool("FollowingPath", false);
                            yield break;
                        }
                    }
                    else
                        yield return null;
			    }
		    }
        }
	}

	IEnumerator Charge()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    weaponAnim.SetBool("Charging", true);
		    int rise, run; 
		    if(player.CurrentNode.gridY - currentNode.gridY > 0)
			    rise = 1;
		    else if(player.CurrentNode.gridY - currentNode.gridY < 0)
			    rise = -1;
		    else 
			    rise = 0;
		    if(player.CurrentNode.gridX - currentNode.gridX > 0)
			    run = 1;
		    else if(player.CurrentNode.gridX - currentNode.gridX < 0)
			    run = -1;
		    else 
			    run = 0;

		    CurrentWaypoint = NodeManager.Instance.grid[CurrentNode.gridX + run, CurrentNode.gridY + rise];
		    while(CurrentWaypoint.CreatureOnNode == null && CurrentWaypoint.CreatureOnNode != this && CurrentWaypoint.walkable == true)
		    {
                if (!GUIManager.Instance.GamePaused)
                {
                    currentWaypoint.CreatureOnNode = this;
                    transform.eulerAngles = TurnTowards(CurrentWaypoint.worldPosition);
                    while (transform.position != CurrentWaypoint.worldPosition)
                    {
                        if (!GUIManager.Instance.GamePaused)
                        {
                            if (!isDisabled)
                            {
                                transform.position = Vector3.MoveTowards(transform.position, CurrentWaypoint.worldPosition, chargeSpeed * Time.deltaTime);
                                yield return null;
                            }
                            else
                            {
                                weaponAnim.SetBool("Charging", false);
                                yield break;
                            }
                        }
                        else
                            yield return null;
                    }
                    CurrentNode.CreatureOnNode = null;
                    CurrentNode = CurrentWaypoint;
                    CurrentWaypoint = NodeManager.Instance.grid[CurrentNode.gridX + run, CurrentNode.gridY + rise];
                    if (currentWaypoint.CreatureOnNode != null)
                    {
                        if (currentWaypoint.CreatureOnNode.GetType().IsSubclassOf(typeof(Enemy)))
                        {
                            GameObject.Destroy(currentWaypoint.CreatureOnNode.gameObject);
                            GUIManager.Instance.UpdateEnemyCount();
                            currentWaypoint = null;
                            isFollowingPath = false;
                            weaponAnim.SetBool("Charging", false);
                            weaponAnim.SetBool("FollowingPath", false);
                            RequestPath(transform.position, player.CurrentNode.worldPosition);
                            yield break;
                        }
                        else
                        {
                            player.TakeDamage(damage * 2);
                        }
                    }
                }
                else
                    yield return null;
		    }
		    isFollowingPath = false;
		    weaponAnim.SetBool("Charging", false);
		    weaponAnim.SetBool("FollowingPath", false);
		    RequestPath (transform.position, player.CurrentNode.worldPosition);
        }
	}
		
	//Returns true if the player is in a straight line on any cardinal or intercardinal direction, no enemies are in the way, and no traps along the path are active. 
	private bool ChargeCheck()
	{
		int rise = player.CurrentNode.gridY - currentNode.gridY;
		int run = player.CurrentNode.gridX - currentNode.gridX;
		Node n;
		//If the player is horizontal to the enemy
		if(rise == 0)
		{
			while(run > 1 || run < -1)
			{
				if (run > 1)
					run--;
				else if (run < -1)
					run++;
				
				n = NodeManager.Instance.grid [currentNode.gridX + run, currentNode.gridY];
				if (n.CreatureOnNode != null || !n.walkable || n.trapActive)
				{
					if(n.CreatureOnNode != player)
						return false;
				}
			}
			return true;
		}
		

		//If the player is vertical to the enemy
		else if(run == 0)
		{
			while(rise > 1 || rise < -1)
			{
				if (rise > 1)
					rise--;
				else if (rise < -1)
					rise++;
				
				n = NodeManager.Instance.grid [currentNode.gridX, currentNode.gridY + rise];
				if (n.CreatureOnNode != null || !n.walkable || n.trapActive) 
				{
					if (n.CreatureOnNode != player)
						return false;
				}
			}
			return true;
		}

		//If the player is diagonal to the enemy
		else if(rise % run == 0 && Mathf.Abs(rise/run) == 1) 
		{
			while(rise > 1 || rise < -1 && run > 1 || run < -1)
			{
				if (rise > 1)
					rise--;
				else if (rise < -1)
					rise++;
				if (run > 1)
					run--;
				else if (run < -1)
					run++;
				
				n = NodeManager.Instance.grid [currentNode.gridX + run, currentNode.gridY + (rise)];
				if (n.CreatureOnNode != null || !n.walkable || n.trapActive)
				{
					if (n.CreatureOnNode != player)
						return false; 
				}
			}
			return true;
		}
		return false;
	}

	IEnumerator AttackPlayer()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    float attackCooldown = 0; 
		    weaponAnim.SetBool("Attacking", true);
		    while(targetNode.CreatureOnNode != null && targetNode.CreatureOnNode.GetType() == typeof(PlayerController))
		    {
                if (!GUIManager.Instance.GamePaused)
                {
                    if (!NodeManager.Instance.CheckAdjacency(currentNode, targetNode))
                    {
                        weaponAnim.SetBool("Attacking", false);
                        RequestPath(transform.position, player.CurrentNode.worldPosition);
                        yield break;
                    }
                    else if (attackCooldown <= 0)
                    {
                        //				Attack (targetNode.CreatureOnNode);
                        attackCooldown = attackSpeed;
                        yield return null;
                    }
                    else
                    {
                        attackCooldown -= Time.deltaTime;
                        yield return null;
                    }
                }
                else
                    yield return null;
		    }
		    weaponAnim.SetBool("Attacking", false);
		    RequestPath (transform.position, player.CurrentNode.worldPosition);
        }
	}

	protected override void RequestPath(Vector3 startPos, Vector3 targetPos)
	{
		Node startNode = NodeManager.Instance.NodeFromWorldPoint (startPos);
		targetNode = NodeManager.Instance.NodeFromWorldPoint (targetPos);
		if (NodeManager.Instance.CheckAdjacency (startNode, targetNode))
		{
			if (targetNode.CreatureOnNode != null && targetNode.CreatureOnNode != this)
			{
//				Attack (targetNode.CreatureOnNode);
				if(player != null)
				{
					PathRequestManager.Instance.RequestPath (this, startNode, targetNode, OnPathFound);
				}
			}
		}
		else 
			PathRequestManager.Instance.RequestPath (this, startNode, targetNode, OnPathFound);
	}

	public override void Attack(Creature c)
	{
		if(c == player)
		{
			if(TargetNode.CreatureOnNode == c)
			{
				if(NodeManager.Instance.CheckAdjacency(TargetNode, CurrentNode))
					player.TakeDamage(damage);
			}
		}
	}

	private Vector3 GetTurnDirection(int delX, int delY)
	{
		Vector3 turnDirection = Vector3.zero;
		if(delX == 0)
		{
			if(player.transform.position.x - transform.position.x < 0)
			{
				turnDirection.x = -1;
				turnDirection.z = 0;
			}
			else if(player.transform.position.x - transform.position.x > 0)
			{
				turnDirection.x = 1;
				turnDirection.z = 0;
			}
		}
		else if(delY == 0)
		{
			if(player.transform.position.z - transform.position.z < 0)
			{
				turnDirection.x = 0;
				turnDirection.z = -1;
			}
			else if(player.transform.position.z - transform.position.z > 0)
			{
				turnDirection.x = 0;
				turnDirection.z = 1;
			}
		}
		return turnDirection;
	}

	Vector3 TurnTowards(Vector3 targetPos)
	{
		Vector3 direction;
		Vector3 lookDir = Vector3.zero;
		direction = (targetPos - transform.position).normalized;	
		float theta = Vector2.Angle (Vector2.up, new Vector2 (direction.x, direction.z));
		if (theta >= 0f && theta < 22.5f)
			lookDir = new Vector3(0,0,0);
		else if (theta >= 167.5f && theta <= 180f)
			lookDir = new Vector3(0,180,0);
		if(direction.x > 0)
		{
			if (theta >= 22.5f && theta < 67.5f)
				lookDir = new Vector3(0,45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,135,0);
		}
		else
			if (theta >= 22.5f && theta < 67.5f)
				lookDir =  new Vector3(0,-45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,-90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,-135,0);

		return lookDir;
	}

	public override void Disable(float duration)
	{
		IsDisabled = true;
        StopAllCoroutines();
	}

	void OnDestroy()
	{
		currentNode.CreatureOnNode = null;
	}
}
