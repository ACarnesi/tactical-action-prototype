﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Creature : MonoBehaviour {
	public float speed;
	protected float knockBackSpeed;
	protected float teleCooldown = 0;
	public int knockBackPower;

	[SerializeField]protected bool isFollowingPath;
	[SerializeField]protected bool isDisabled;
	[SerializeField]protected bool isHooked;
	protected float step;
	[SerializeField]protected Node currentWaypoint;
	[SerializeField]protected Node currentNode;
	protected Node targetNode;
	protected ICommand currentCommand;
	protected ICommand nextCommand;
	[SerializeField]protected Creature targetCreature;

	public Creature TargetCreature{
		get{return targetCreature;}
		set{targetCreature = value;}
	}

	public Node CurrentNode{
		get{return currentNode;}
		set
		{
			currentNode = value;
		}
	}

	public Node TargetNode{
		get{return targetNode;}
		set
		{
			targetNode = value;
		}
	}

	public bool IsDisabled{
		get{return isDisabled;}
		set{isDisabled = value;
			if(value)
			{
				isFollowingPath = false;
				if(CurrentWaypoint != CurrentNode)
					currentWaypoint.CreatureOnNode = null;
			}
		}
	}

	public bool IsHooked
	{
		get{return isHooked;}
		set
		{
			isHooked = value;
		}
	}

	public bool IsFollowingPath{
		get{return isFollowingPath;}
		set{isFollowingPath = value;}
	}
		
	public Node CurrentWaypoint
	{
		get{return currentWaypoint;}
		set{currentWaypoint = value;}
	}

	public float TeleCooldown{
		get{return teleCooldown;}
		set{teleCooldown = value;}
	}

	protected void Start()
	{
		CurrentNode = NodeManager.Instance.NodeFromWorldPoint (transform.position);
		knockBackSpeed = 5;
		CurrentNode.CreatureOnNode = this;
	}

	private IEnumerator Knockback(Vector2 direction, int power)
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    Node nextNode;
		    isDisabled = true;
		    if (currentCommand != null)
			    currentCommand.CancelCommand ();
		    if (isFollowingPath)
		    {
			    isFollowingPath = false;
			    nextNode = NodeManager.Instance.grid [currentWaypoint.gridX + (int)direction.x, currentWaypoint.gridY + (int)direction.y];
			    if(currentWaypoint.CreatureOnNode == this && currentWaypoint != currentNode)
			    {
				    currentWaypoint.CreatureOnNode = null;
			    }
		    }
		    else
			    nextNode = NodeManager.Instance.grid [currentNode.gridX + (int)direction.x, currentNode.gridY + (int)direction.y];
		    while(power > 0)
		    {
                if (!GUIManager.Instance.GamePaused)
                {
                    if (nextNode.walkable)
                    {
                        if (nextNode.CreatureOnNode != null && nextNode.CreatureOnNode != this)
                        {
                            power -= 1;
                            if (power > 0)
                            {
                                nextNode.CreatureOnNode.ApplyKnockback(direction, power);
                                break;
                            }
                            else
                                break;
                        }
                        else
                        {
                            nextNode.CreatureOnNode = this;
                            power -= 1;
                            while (transform.position != nextNode.worldPosition)
                            {
                                if (!GUIManager.Instance.GamePaused)
                                {
                                    if (!isHooked)
                                    {
                                        step = knockBackSpeed * Time.deltaTime;
                                        transform.position = Vector3.MoveTowards(transform.position, nextNode.worldPosition, step);
                                        yield return null;
                                    }
                                    else
                                    {
                                        nextNode.CreatureOnNode = null;
                                        transform.position = currentNode.worldPosition;
                                        yield break;
                                    }
                                }
                                else
                                    yield return null;
                            }
                            if (currentNode != nextNode)
                                currentNode.CreatureOnNode = null;
                            currentNode = nextNode;
                            nextNode = NodeManager.Instance.grid[currentNode.gridX + (int)direction.x, currentNode.gridY + (int)direction.y];
                        }
                    }
                    else
                        break;
                }
                else
                    yield return null;
		    }
		    isDisabled = false;
		    if(this.GetType() != typeof(PlayerController))
		    {
			    GameObject player = GameObject.FindGameObjectWithTag ("Player");
			    RequestPath (transform.position, player.transform.position);
		    }
        }
	}

	public void ApplyKnockback(Vector2 direction, int power)
	{
		if (!isDisabled)
		{
			StopCoroutine ("PursueTarget");
			StartCoroutine(Knockback(direction, power));
		}
	}

	public abstract void Attack (Creature c);

	protected abstract void RequestPath (Vector3 startPos, Vector3 targetPos);

	public abstract void OnPathFound(List<Node> newPath, bool pathSuccessful);

	public abstract void Disable (float duration);
}
