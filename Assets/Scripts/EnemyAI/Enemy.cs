﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Creature {

	// Use this for initialization
	new void Start () {
		base.Start ();
	}

	public override void Attack(Creature c)
	{
		
	}

	protected override void RequestPath (Vector3 startPos, Vector3 targetPos)
	{
		
	}

	public override void OnPathFound(List<Node> newPath, bool pathSuccessful)
	{
		
	}

	public override void Disable(float duration)
	{

	}

}
