﻿using UnityEngine;
using System.Collections;

public class TeleportCommand : ICommand {

	private Node targetNode;
	private Creature creature;
	private bool shrinking;
	private bool beganExecution;

	//A move command must be constructed with a target position. 
	public TeleportCommand(Creature c, Vector3 tarPos)
	{
		targetNode = NodeManager.Instance.NodeFromWorldPoint(tarPos);
		creature = c;
		beganExecution = false;
		shrinking = true;
	}

	//Moves the ccter towards the target position. Returns true when within acceptable range of target position.
	public override bool ExecuteCommand()
	{
		if(!beganExecution)
		{
			if (targetNode.CreatureOnNode == null) {
				creature.TeleCooldown = 3.0f;
				targetNode.CreatureOnNode = creature;
				beganExecution = true;
			} else
				return true;
		}

		Vector3 newScale;
		if(shrinking)
		{
			newScale = creature.transform.localScale * .9f;
			if (newScale.z >= .05f) {
				creature.transform.localScale = newScale;
				return false;
			} else
			{
				shrinking = false;
				creature.CurrentNode.CreatureOnNode = null;
				creature.CurrentNode = targetNode;
				creature.transform.position = targetNode.worldPosition;
				return false;
			}
		}
		else
		{
			newScale = creature.transform.localScale / .9f;
			if (newScale.z <= 1f) {
				creature.transform.localScale = newScale;
				return false;
			} else
				return true;
		}
	}

	public override void CancelCommand()
	{
		creature.IsFollowingPath = false;
		targetNode.CreatureOnNode = null;
		creature.transform.localScale = Vector3.one;
		creature.transform.position = creature.CurrentNode.worldPosition;
		creature.CurrentWaypoint.CreatureOnNode = null;
		creature.CurrentWaypoint = null;
	}
}
