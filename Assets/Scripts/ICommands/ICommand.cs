﻿using UnityEngine;
using System.Collections;

//An abstract class for commands to inherit from.
public abstract class ICommand {
	public abstract bool ExecuteCommand();
	public abstract void CancelCommand();
}
