﻿using UnityEngine;
using System.Collections;

public class MoveCommand : ICommand {

	private bool beganExecution;
	private Vector3 lookDir;
	private Vector3 targetPos;
	private Creature creature;

	//A move command must be constructed with a target position. 
	public MoveCommand(Creature c, Vector3 tarPos)
	{
		beganExecution = false;
		targetPos = tarPos;
		creature = c;
		Vector3 direction;
		direction = (targetPos - creature.transform.position).normalized;	
		float theta = Vector2.Angle (Vector2.up, new Vector2 (direction.x, direction.z));
		if (theta >= 0f && theta < 22.5f)
			lookDir = new Vector3(0,0,0);
		else if (theta >= 167.5f && theta <= 180f)
			lookDir = new Vector3(0,180,0);
		if(direction.x > 0)
		{
			if (theta >= 22.5f && theta < 67.5f)
				lookDir = new Vector3(0,45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,135,0);
		}
		else
			if (theta >= 22.5f && theta < 67.5f)
				lookDir =  new Vector3(0,-45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,-90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,-135,0);
	}

	//Moves the ccter towards the target position. Returns true when within acceptable range of target position.
	public override bool ExecuteCommand()
	{
		if(!beganExecution)
		{
			creature.transform.eulerAngles = lookDir;
			beganExecution = true;
		}

		float distMoved = creature.speed * Time.deltaTime;
		if(distMoved < (Vector3.Magnitude(targetPos - creature.transform.position)))
		{
			Vector3 direction = (targetPos - creature.transform.position).normalized;
			NodeManager.Instance.NodeFromWorldPoint (targetPos);
			creature.transform.position += distMoved * direction;
			return false;
		}

		else
		{
			creature.transform.position = targetPos;
			return true;
		}
	}
		
	public override void CancelCommand()
	{

	}
}
