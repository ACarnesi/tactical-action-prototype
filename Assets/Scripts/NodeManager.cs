﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NodeManager : MonoBehaviour {

	public bool debugEnabled = false;
	public Transform player;
	public LayerMask unwalkable;
	public Vector2 gridSize;
	public float nodeRadius;
	public Node[,] grid; 

	private float nodeDiameter;
	[SerializeField]private int gridSizeX, gridSizeY;

	private static NodeManager instance;
	public static NodeManager Instance
	{
		get { 
			if (instance != null )
				return instance;
			else
				return instance = new GameObject ("NodeManager").AddComponent<NodeManager> ();
		}
	}

	void Awake(){
		if(GameObject.FindGameObjectWithTag("NodeManager") != null)
			instance = this;
		nodeDiameter = nodeRadius * 2;
		//Calculates number of tiles based on node size & gridSize
		gridSizeX = (int)(gridSize.x / nodeDiameter);
		gridSizeY = (int)(gridSize.y / nodeDiameter);
		CreateGrid();
	}

	public int MaxSize{
		get{
			return gridSizeX * gridSizeY;
		}
	}

	void CreateGrid(){
		//Initializes the grid of nodes and populates it starting from the bottom left of the world
		grid = new Node[gridSizeX, gridSizeY];
		Vector3 worldBottomLeft = transform.position - Vector3.right * gridSize.x/2 - Vector3.forward * gridSize.y/2;
		for(int x = 0; x < gridSizeX; x++){
			for(int y = 0; y < gridSizeY; y++){
				Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
				//checks tile for obstruction 
				bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkable));
				grid[x,y] = new Node(walkable, worldPoint, x, y);
			}
		}
	}
	 
	public List<Node> GetNeighbours(Node n){
		List<Node> neighbours = new List<Node>();

		for(int x = -1; x <= 1; x++){
			for(int y = -1; y <= 1; y++){
				if(x == 0 && y == 0)
					continue;

				int checkX = n.gridX + x;
				int checkY = n.gridY + y;

				if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY){
					neighbours.Add(grid[checkX,checkY]);
				}
			}
		}

		return neighbours;
	}

	//returns a node based on a specified world point. 
	public Node NodeFromWorldPoint(Vector3 worldPosition){
		//Percentage values of where the point lies in relation to the grid size. 
		float percentX = (worldPosition.x + gridSize.x/2 - transform.position.x) / gridSize.x;
		float percentY = (worldPosition.z + gridSize.y/2 - transform.position.z) / gridSize.y;
		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);

		int x = (int)((gridSizeX) * percentX);
		int y = (int)((gridSizeY) * percentY);
		return grid[x,y];
	}

	void OnDrawGizmos(){
		if(debugEnabled){
			Gizmos.DrawWireCube(transform.position, new Vector3(gridSize.x, 1, gridSize.y));
			
			if(grid != null){
				foreach (Node n in grid){
					Gizmos.color = (n.walkable)?Color.white:Color.red;
					if (n.CreatureOnNode != null)
						Gizmos.color = Color.blue;
					else if (n.trapActive)
						Gizmos.color = Color.magenta;
					else if (n.containsTrap)
						Gizmos.color = Color.gray;
					Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - .1f));
				}
			}
		}
	}

	public bool CheckAdjacency(Node start, Node end)
	{
		int deltaX = start.gridX - end.gridX;
		int deltaY = start.gridY - end.gridY;

		return (Mathf.Abs(deltaX) <= 1 && Mathf.Abs(deltaY) <= 1)?true:false;
	}

	public int NodeDistance(Node origin, Node target)
	{
		int deltaX = target.gridX - origin.gridX;
		int deltaY = target.gridY - origin.gridY;
		return (Mathf.Abs(deltaX) + Mathf.Abs(deltaY));
	}
}
