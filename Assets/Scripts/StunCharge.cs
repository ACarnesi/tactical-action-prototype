﻿using UnityEngine;
using System.Collections;

public class StunCharge : MonoBehaviour {

	public float speed = 3;

	private float dmgAmount = 2.0f;
	private Vector2 targetDir;
	private Node currentNode;

	void Update()
	{
		if(currentNode.walkable)
		{
			if(transform.position == currentNode.worldPosition)
			{
				if(currentNode.creature != null && currentNode.creature.tag == "Player")
				{
					
					PlayerController player = currentNode.CreatureOnNode.GetComponent<PlayerController>();
					player.TakeDamage(dmgAmount);
					currentNode.CreatureOnNode.Disable (.5f);
					Destroy(this.gameObject);
				}
				currentNode = PickNextNode();
			}
			else
			{
				transform.position = Vector3.MoveTowards(transform.position, currentNode.worldPosition, speed * Time.deltaTime);
			}
		}
		else
			Destroy(this.gameObject);
	}

	public void Initialize(Node originNode, Vector2 direction)
	{
		targetDir = direction;
		currentNode = originNode;
	}

	private Node PickNextNode()
	{
//		Random.InitState(Random.Range(0,1001));
		int delX, delY;
		if(targetDir.x == 0)
		{
			delX = Random.Range(-1,2);
			if(targetDir.y == 1)
				delY = 1;
			else
				delY = -1;
		}

		else if(targetDir.y == 0)
		{
			delY = Random.Range(-1,2);
			if(targetDir.x == 1)
				delX = 1;
			else
				delX = -1;
		}

		else
		{
			if(targetDir.x == 1)
			{
				delX = Random.Range(0,2);
			}
			else
			{
				delX = Random.Range(-1,1);
			}
			
			if(targetDir.y == 1)
			{
				delY = Random.Range(0,2);
			}
			else
			{
				delY = Random.Range(-1,1);
			}
		}

		return NodeManager.Instance.grid[currentNode.gridX + delX, currentNode.gridY + delY];
	}
}
