﻿using UnityEngine;
using System.Collections;

public class SwordScript : MonoBehaviour {

	Creature weilder;

	// Use this for initialization
	void Start () {
		weilder = GetComponentInParent<Creature>();
	}

	public void FireAttack()
	{
		weilder.Attack(weilder.TargetNode.CreatureOnNode);
	}
}
