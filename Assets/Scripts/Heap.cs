﻿using UnityEngine;
using System;
using System.Collections;

public class Heap<T> where T : IHeapItem<T> {

	//The heap is ordered like an upside down tree sorted from lowest value to highest value. 
	//Each item has two children, found by multiplying its own index by 2 and adding 1 for the left child, and 2 for the right child. 
	//The parent of an item is found by subtracting 1 from its own index and dividng by two. 

	T[] items;
	int currentItemCount;

	public Heap(int maxHeapSize) {
		items = new T[maxHeapSize];
	}

	//Adds a new item into the next index in the item array and then sorts it to its appropriate position since it will be placed at the end of the heap
	public void Add(T item)
	{
		item.HeapIndex = currentItemCount;
		items[currentItemCount] = item;
		SortUp(item);
		currentItemCount++;
	}

	//Returns the first item in the items array and replaces it with the last item in the items array. 
	//Sorts the replaced item down to its appropriate position. 
	public T RemoveFirst()
	{
		T firstItem = items[0];
		currentItemCount--;
		items[0] = items[currentItemCount];
		items[0].HeapIndex = 0;
		SortDown(items[0]);
		return firstItem;
	}
		
	public void UpdateItem(T item)
	{
		SortUp(item);
	}

	public int Count 
	{
		get 
		{
			return currentItemCount;
		}
	}

	public bool Contains(T item) 
	{
		return Equals(items[item.HeapIndex], item);
	}

	void SortDown(T item)
	{
		while(true)
		{
			int childIndexLeft = item.HeapIndex * 2 + 1;
			int childIndexRight = item.HeapIndex * 2 + 2;
			int swapIndex = 0;

			if(childIndexLeft < currentItemCount)
			{
				swapIndex = childIndexLeft;

				if(childIndexRight < currentItemCount)
				{
					if(items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
					{
						swapIndex = childIndexRight;
					}
				}

				if(item.CompareTo(items[swapIndex]) < 0) 
				{
					Swap(item,items[swapIndex]);
				}
				else
				{
					return;
				}
			}
			else
			{
				return;
			}
		}
	}

	void SortUp(T item)
	{
		int parentIndex = (item.HeapIndex-1)/2;

		while(true)
		{
			T parentItem = items[parentIndex];
			if(item.CompareTo(parentItem) > 0)
			{
				Swap(item, parentItem);
			}
			else
			{
				break;
			}

			parentIndex = (item.HeapIndex-1)/2;
		}
	}

	void Swap(T itemA, T itemB)
	{
		items[itemA.HeapIndex] = itemB;
		items[itemB.HeapIndex] = itemA;
		int itemAIndex = itemA.HeapIndex;
		itemA.HeapIndex = itemB.HeapIndex;
		itemB.HeapIndex = itemAIndex;
	}
}

public interface IHeapItem<T> : IComparable<T> 
{
	int HeapIndex 
	{
		get;
		set;
	}
}
