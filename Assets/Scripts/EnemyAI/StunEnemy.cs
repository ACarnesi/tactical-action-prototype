﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StunEnemy : Enemy {

	public int retreatDist; 
	public int teleportDist;
	public int stunCharges;
	public float activationDist;
	public float attackSpeed;
	public float teleportCooldown;
	public LayerMask enemyMask;

	private bool isActive;
	private List<Node> path = new List<Node> ();
	private int targetIndex;
	private float attackCooldown;
	private PlayerController player;
	private Object stunChargePrefab;

	private bool IsActive
	{
		set
		{
			isActive = value;
			if(value == true)
			{
				RequestPath (currentNode.worldPosition, player.CurrentNode.worldPosition);
			}
		}
	}

	new void Start () 
	{
		base.Start ();
		stunChargePrefab = Resources.Load("Prefabs/StunChargePrefab");
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
	}

	void Update()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    UpdateCooldowns();
		    if(!isActive)
		    {
			    Vector3 playerDir = (player.transform.position - transform.position).normalized;
			    RaycastHit hitInfo;
			    if(Physics.Raycast(transform.position, playerDir, out hitInfo, activationDist, enemyMask))
			    {
				    if(hitInfo.collider.tag == "Player")
					    IsActive = true;
			    }
		    } 
		    else if(!isFollowingPath && !isDisabled)
		    {
			    RequestPath (transform.position, player.CurrentNode.worldPosition);
		    }
        }
	}

	public override void OnPathFound(List<Node> newPath, bool pathSuccessful){
		if (!isDisabled) {
			if(pathSuccessful){
				//If the player is already following a path, continue its movement to its nextNode and append the new path to the end of that. 
				if(!isFollowingPath){
					isFollowingPath = true;
					path = newPath;
					targetIndex = 0;
					StartCoroutine("PursueTarget");
				}
			}
		}
	}

	IEnumerator PursueTarget()
	{
		CurrentWaypoint = path [targetIndex];
		while(true)
		{
            if (!GUIManager.Instance.GamePaused)
            {
                Vector3 playerDir = (player.transform.position - transform.position).normalized;
                RaycastHit hitInfo;
                if (Physics.Raycast(transform.position, playerDir, out hitInfo, 100, enemyMask))
                {
                    if (hitInfo.collider.tag == "Player")
                    {
                        //If the playre is too close, teleport away. If the player is too far away, teleport towards them until you get to them. 
                        int playerDist = NodeManager.Instance.NodeDistance(CurrentNode, player.CurrentNode);
                        if (playerDist <= 3)
                        {
                            if (TeleCooldown <= 0)
                            {
                                CurrentWaypoint = FindTeleportLocation(true);
                                currentCommand = new TeleportCommand(this, CurrentWaypoint.worldPosition);
                                while (true)
                                {
                                    if (!GUIManager.Instance.GamePaused)
                                    {
                                        if (!currentCommand.ExecuteCommand())
                                            yield return null;
                                        else
                                        {
                                            currentCommand = null;
                                            break;
                                        }
                                    }
                                    else yield return null;
                                }
                            }
                            else
                                Attack(player);
                        }
                        else
                            Attack(player);
                    }
                    else
                    {
                        if (TeleCooldown <= 0)
                        {
                            CurrentWaypoint = FindTeleportLocation(false);
                            currentCommand = new TeleportCommand(this, CurrentWaypoint.worldPosition);
                            while (true)
                            {
                                if (!GUIManager.Instance.GamePaused)
                                {
                                    if (!IsDisabled)
                                    {
                                        if (!currentCommand.ExecuteCommand())
                                            yield return null;
                                        else
                                        {
                                            currentCommand = null;
                                            break;
                                        }
                                    }
                                }
                                else
                                    yield return null;
                            }
                        }
                    }
                }
                if (player.CurrentNode != targetNode)
                {
                    RequestPath(transform.position, player.CurrentNode.worldPosition);
                    isFollowingPath = false;
                    yield break;
                }
                yield return null;
            }
            else
                yield return null;
		}
	}

	private void UpdateCooldowns()
	{
		if(attackCooldown > 0)
			attackCooldown -= Time.deltaTime;
		if(teleCooldown > 0)
			teleCooldown -= Time.deltaTime;
	}

	private Node FindTeleportLocation(bool playerVisible)
	{
		Node bestNode = CurrentNode;
		int nodesSearched = teleportDist;
		if(playerVisible)
		{
			while(nodesSearched > 0)
			{
				List<Node> neighbours = NodeManager.Instance.GetNeighbours (bestNode);
				float bestDistance = Mathf.Abs(Vector3.Distance(bestNode.worldPosition, player.CurrentNode.worldPosition));
				foreach(Node n in neighbours) 
				{
					if (n.CreatureOnNode != null || n.walkable == false || n.containsTrap == true)
						continue;
					else if (Mathf.Abs (Vector3.Distance (n.worldPosition, player.CurrentNode.worldPosition)) > bestDistance)
					{
						bestNode = n;
						bestDistance = Mathf.Abs (Vector3.Distance (n.worldPosition, player.CurrentNode.worldPosition));
					}
				}
				nodesSearched--;
			}
		}
		else
		{
			while(nodesSearched > 0)
			{
				List<Node> neighbours = NodeManager.Instance.GetNeighbours (bestNode);
				float bestDistance = Mathf.Abs(Vector3.Distance(bestNode.worldPosition, CurrentWaypoint.worldPosition));
				foreach(Node n in neighbours) 
				{
					if (n.CreatureOnNode != null || n.walkable == false || n.trapActive == true)
						continue;
					else if (Mathf.Abs (Vector3.Distance (n.worldPosition, CurrentWaypoint.worldPosition)) < bestDistance)
					{
						bestNode = n;
						bestDistance = Mathf.Abs (Vector3.Distance (n.worldPosition, CurrentWaypoint.worldPosition));
					}
				}
				nodesSearched--;
				if(bestNode == CurrentWaypoint)
				{
					targetIndex++;
					if (targetIndex < path.Capacity)
						CurrentWaypoint = path [targetIndex];
				}
			}
		}
		return bestNode;
	}

	protected override void RequestPath(Vector3 startPos, Vector3 targetPos)
	{
		Node startNode = NodeManager.Instance.NodeFromWorldPoint (startPos);
		targetNode = NodeManager.Instance.NodeFromWorldPoint (targetPos);
		PathRequestManager.Instance.RequestPath (this, startNode, targetNode, OnPathFound);
	}

	public override void Attack(Creature c)
	{
		if(attackCooldown <= 0)
		{
			Vector2 attackDir;
			if (c.CurrentNode.gridX - currentNode.gridX > 0)
				attackDir.x = 1;
			else if (c.CurrentNode.gridX - currentNode.gridX < 0)
				attackDir.x = -1;
			else
				attackDir.x = 0;

			if (c.CurrentNode.gridY - currentNode.gridY > 0)
				attackDir.y = 1;
			else if (c.CurrentNode.gridY - currentNode.gridY < 0)
				attackDir.y = -1;
			else
				attackDir.y = 0;

			int charges = stunCharges;
			for(int i = 0; i < charges; i++)
			{
				GameObject stunCharge = Instantiate(stunChargePrefab, transform.position, Quaternion.Euler(-90,0,0)) as GameObject;
				StunCharge s = stunCharge.GetComponent<StunCharge> ();
				s.Initialize (CurrentNode, attackDir);
			}
			attackCooldown = 2f;
		}
	}

	public override void Disable(float duration)
	{
		IsDisabled = true;
	}

	void OnDestroy()
	{
		currentNode.CreatureOnNode = null;
	}
}
