﻿using UnityEngine;
using System.Collections;

public class PathRequestCommand : ICommand {

	private Node startNode, targetNode;
	private Creature creature;

	//A move command must be constructed with a target position. 
	public PathRequestCommand(Creature c, Node targetN)
	{
		targetNode = targetN;
		creature = c;
	}

	//Moves the ccter towards the target position. Returns true when within acceptable range of target position.
	public override bool ExecuteCommand()
	{
		startNode = NodeManager.Instance.NodeFromWorldPoint (creature.transform.position);
		if (targetNode.CreatureOnNode != null)
			creature.TargetCreature = targetNode.CreatureOnNode;
		else 
		{
			creature.TargetCreature = null;
		}

		if (NodeManager.Instance.CheckAdjacency (startNode, targetNode))
		{
			if (targetNode.CreatureOnNode != null && targetNode.CreatureOnNode != creature)
				creature.Attack (targetNode.CreatureOnNode);
			else
				PathRequestManager.Instance.RequestPath (creature, startNode, targetNode,  creature.OnPathFound);
		}
		else 
			PathRequestManager.Instance.RequestPath (creature, startNode, targetNode, creature.OnPathFound);

		return true;
	}

	public override void CancelCommand()
	{

	}
}
