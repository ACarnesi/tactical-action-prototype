﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour {
    
	[SerializeField]Image dashCooldown, hookshotCooldown, teleportCooldown;
	[SerializeField]Slider healthSlider;
	[SerializeField]Text GameOverText, EnemiesRemainingText;
	[SerializeField]Button RestartButton;

    [SerializeField]bool gamePaused;
	int enemiesRemaining;
	PlayerController player;

    private static GUIManager instance;
    public static GUIManager Instance
    {
        get
        {
            if (instance != null)
                return instance;
            else
                return instance = new GameObject("GUI").AddComponent<GUIManager>();
        }
    }

    public bool GamePaused
    {
        get{return gamePaused;}
        set {gamePaused = value;}
    }

    private void Awake()
    {
        if (GameObject.FindGameObjectWithTag("GUI") != null)
            instance = this;
        Time.timeScale = 1.0f;
    }

    public float DashCooldown
	{
		get{return dashCooldown.fillAmount;}
		set{dashCooldown.fillAmount = value / 5;}
	}

	public float HookshotCooldown
	{
		get{return hookshotCooldown.fillAmount;}
		set{hookshotCooldown.fillAmount = value / 3;}
	}

	public float TeleportCooldown
	{
		get{return teleportCooldown.fillAmount;}
		set{teleportCooldown.fillAmount = value / 3;}
	}

	public float HealthSlider
	{
		get{return healthSlider.value;}
		set{healthSlider.value = value;}
	}

	void Start()
	{
        GamePaused = false;
		dashCooldown.fillAmount = 0;
		hookshotCooldown.fillAmount = 0;
		teleportCooldown.fillAmount = 0;
		GameObject[] creatures = GameObject.FindGameObjectsWithTag("Enemy");
		enemiesRemaining = creatures.Length;
		EnemiesRemainingText.text = "Enemies Remaining: " + enemiesRemaining.ToString();
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		GameOverText.enabled = false;
		RestartButton.gameObject.SetActive(false);
	}

	public void UpdateEnemyCount()
	{
		enemiesRemaining--;
		EnemiesRemainingText.text = "Enemies Remaining: " + enemiesRemaining.ToString();
		if(enemiesRemaining <= 0)
		{
			GameOverText.text = "You Win!";
			GameOverText.color = Color.yellow;
			GameOver();
		}
	}

	public void GameOver()
	{
		GameOverText.enabled = true;
		RestartButton.gameObject.SetActive(true);
        GamePaused = true;
		Time.timeScale = 0;
	}

	public void RestartButtonPress()
	{
		//#if UNITY_EDITOR
		//UnityEditor.EditorApplication.isPlaying = false;
		//#else
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		//#endif 
	}
}