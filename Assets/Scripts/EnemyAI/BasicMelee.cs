﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicMelee : Enemy {

	public float activationDist;
	public float attackSpeed;
	public LayerMask enemyMask;

	private bool isActive;
	private float damage;
	private List<Node> path = new List<Node> ();
	private PlayerController player;
	private Animator weaponAnim;


	private bool IsActive
	{
		set
		{
			isActive = value;
			if(value == true)
			{
				if (!isFollowingPath)
					RequestPath (transform.position, player.CurrentNode.worldPosition);
				else
					RequestPath (currentNode.worldPosition, player.CurrentNode.worldPosition);
			}
		}
	}

	new void Start () 
	{
		base.Start ();
		damage = 5.0f;
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
		weaponAnim = GetComponentInChildren<Animator>();
	}

	void Update()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    if(!isActive)
		    {
			    Vector3 playerDir = (player.transform.position - transform.position).normalized;
			    RaycastHit hitInfo;
			    if(Physics.Raycast(transform.position, playerDir, out hitInfo, activationDist, enemyMask))
			    {
				    if(hitInfo.collider.tag == "Player")
					    IsActive = true;
			    }
		    } 
		    else if(!isFollowingPath && !isDisabled)
		    {
			    RequestPath (transform.position, player.CurrentNode.worldPosition);
		    }
        }
	}
		
	public override void OnPathFound(List<Node> newPath, bool pathSuccessful){
		if (!isDisabled) {
			if(pathSuccessful){
				//If the player is already following a path, continue its movement to its nextNode and append the new path to the end of that. 
				if(!isFollowingPath){
					isFollowingPath = true;
					weaponAnim.SetBool("FollowingPath", true);
					path = newPath;
					StartCoroutine("PursueTarget");
				}
				else
				{
					newPath.Insert(0, CurrentWaypoint);
					path = newPath;
				}
			}
		}
	}
	 
	IEnumerator PursueTarget()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    //Move through from waypoint to waypoint until you reach the player, then attack
		    for (int i = 0; i < path.Count; i++) 
		    {
			    Node nextNode;
			    nextNode = path [i];
			    while (transform.position != nextNode.worldPosition && isDisabled == false) 
			    {
                    if (!GUIManager.Instance.GamePaused)
                    {
                        currentWaypoint = currentNode;
                        List<Node> neighbours = NodeManager.Instance.GetNeighbours(currentNode);
                        float bestDistance = Mathf.Abs(Vector3.Distance(currentNode.worldPosition, nextNode.worldPosition));
                        //Find the next closest node to the waypoint. 
                        foreach (Node n in neighbours)
                        {
                            //If one of the nodes contains the target, stop processing pathing and Attack.
                            if (n.CreatureOnNode == player)
                            {
                                isFollowingPath = false;
                                weaponAnim.SetBool("FollowingPath", false);
                                StartCoroutine("AttackPlayer");
                                yield break;
                            }
                            else if (n.CreatureOnNode != null || n.walkable == false)
                                continue;
                            else if (Mathf.Abs(Vector3.Distance(n.worldPosition, nextNode.worldPosition)) < bestDistance)
                            {
                                currentWaypoint = n;
                                bestDistance = Mathf.Abs(Vector3.Distance(n.worldPosition, nextNode.worldPosition));
                            }
                        }

                        //If the closest walkable node was not your own, move to that node. 
                        if (currentWaypoint != currentNode)
                        {
                            transform.eulerAngles = TurnTowards(CurrentWaypoint.worldPosition);
                            CurrentWaypoint.CreatureOnNode = this;
                            while (transform.position != currentWaypoint.worldPosition)
                            {
                                if (!GUIManager.Instance.GamePaused)
                                {
                                    if (!isDisabled)
                                    {
                                        transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.worldPosition, speed * Time.deltaTime);
                                        yield return null;
                                    }
                                    else
                                        yield break;
                                }
                                else
                                    yield return null;
                            }
                            currentNode.CreatureOnNode = null;
                            currentNode = currentWaypoint;
                        }
                        else
                            yield return null;

                        //If the player has changed positions, correct path
                        if (player.CurrentNode != targetNode)
                        {
                            RequestPath(transform.position, player.CurrentNode.worldPosition);
                            weaponAnim.SetBool("FollowingPath", false);
                            isFollowingPath = false;
                            yield break;
                        }
                    }
                    else
                        yield return null;
			    }
		    }
        }
	}
		
	Vector3 TurnTowards(Vector3 targetPos)
	{
		Vector3 direction;
		Vector3 lookDir = Vector3.zero;
		direction = (targetPos - transform.position).normalized;	
		float theta = Vector2.Angle (Vector2.up, new Vector2 (direction.x, direction.z));
		if (theta >= 0f && theta < 22.5f)
			lookDir = new Vector3(0,0,0);
		else if (theta >= 167.5f && theta <= 180f)
			lookDir = new Vector3(0,180,0);
		if(direction.x > 0)
		{
			if (theta >= 22.5f && theta < 67.5f)
				lookDir = new Vector3(0,45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,135,0);
		}
		else
			if (theta >= 22.5f && theta < 67.5f)
				lookDir =  new Vector3(0,-45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,-90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,-135,0);

		return lookDir;
	}

	IEnumerator AttackPlayer()
	{
		weaponAnim.SetBool("Attacking", true);
		float attackCooldown = 0; 
		transform.eulerAngles = TurnTowards(TargetNode.worldPosition);
		while(targetNode.CreatureOnNode != null && targetNode.CreatureOnNode.GetType() == typeof(PlayerController))
		{
            if (!GUIManager.Instance.GamePaused)
            {
                if (!NodeManager.Instance.CheckAdjacency(currentNode, targetNode))
                {
                    weaponAnim.SetBool("Attacking", false);
                    RequestPath(transform.position, player.CurrentNode.worldPosition);
                    yield break;
                }
                else if (attackCooldown <= 0)
                {
                    //				Attack (targetNode.CreatureOnNode);
                    attackCooldown = attackSpeed;
                    yield return null;
                }
                else
                {
                    attackCooldown -= Time.deltaTime;
                    yield return null;
                }
            }
            else
                yield return null;
		}
		weaponAnim.SetBool("Attacking", false);
		RequestPath (transform.position, player.CurrentNode.worldPosition);
	}

	protected override void RequestPath(Vector3 startPos, Vector3 targetPos)
	{
		Node startNode = NodeManager.Instance.NodeFromWorldPoint (startPos);
		targetNode = NodeManager.Instance.NodeFromWorldPoint (targetPos);
		if (NodeManager.Instance.CheckAdjacency (startNode, targetNode))
		{
			if (targetNode.CreatureOnNode != null && targetNode.CreatureOnNode != this)
			{
				StartCoroutine("AttackPlayer");
//				Attack (targetNode.CreatureOnNode);
//				if(player != null)
//				{
//					PathRequestManager.Instance.RequestPath (this, startNode, targetNode, OnPathFound);
//				}
			}
		}
		else 
			PathRequestManager.Instance.RequestPath (this, startNode, targetNode, OnPathFound);
	}

	public override void Attack(Creature c)
	{
		if(c == player)
		{
			if(TargetNode.CreatureOnNode == c)
			{
				if(NodeManager.Instance.CheckAdjacency(TargetNode, CurrentNode))
					player.TakeDamage(damage);
			}
		}
	}

	public override void Disable(float duration)
	{
		IsDisabled = true;
	}

	void OnDestroy()
	{
		currentNode.CreatureOnNode = null;
	}
}
