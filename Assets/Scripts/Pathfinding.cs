﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;

public class Pathfinding : MonoBehaviour {
	public void StartFindPath(Node startPos, Node targetPos, Creature requestee)
	{
		if (requestee.GetType () == typeof(Enemy))
			StartCoroutine (FindPath (startPos, targetPos, requestee));
		else
			StartCoroutine (FindAlternatePath (startPos, targetPos, requestee));
	}

	IEnumerator FindPath(Node startNode, Node targetNode, Creature requestee)
	{
		List<Node> waypoints = new List<Node>();
		bool pathSuccess = false;

		if(startNode == targetNode){
			pathSuccess = false;
			PathRequestManager.Instance.FinishedProcessingPath(waypoints, pathSuccess);
			yield break;
		}

		//Try to find a path if both nodes are walkable
		if(startNode.walkable && targetNode.walkable)
		{
			Heap<Node> openNodes = new Heap<Node>(NodeManager.Instance.MaxSize);
			HashSet<Node> closedNodes = new HashSet<Node>();
			openNodes.Add(startNode);

			//Evaluate all nodes in the openNodes Heap and add evaluated nodes to the closed nodes group
			while(openNodes.Count > 0)
			{
				Node currentNode = openNodes.RemoveFirst();
				closedNodes.Add(currentNode);

				if(currentNode == targetNode)
				{
					pathSuccess = true;
					break;
				}

				//Check every neighbour of the node evaluated for better pathing options (lower fcost)
				foreach (Node neighbour in NodeManager.Instance.GetNeighbours(currentNode))
				{
					if(!neighbour.walkable|| closedNodes.Contains(neighbour) || neighbour.containsTrap || neighbour.CreatureOnNode != null && neighbour != targetNode && neighbour.CreatureOnNode != requestee && neighbour.CreatureOnNode.GetType() != typeof(PlayerController))
					{
						continue;
					}

					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
					if(newMovementCostToNeighbour < neighbour.gCost || !openNodes.Contains(neighbour))
					{
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

						if(!openNodes.Contains(neighbour))
							openNodes.Add(neighbour);					
						else
							openNodes.UpdateItem(neighbour);
					}
				}
			}
		}
		yield return null;
		if (requestee == null)
			yield break;
		if(pathSuccess) 
		{
			waypoints = RetracePath(startNode, targetNode);
			if (requestee.GetType () == typeof(Enemy))
				waypoints = SimplifyPath (waypoints, NodeManager.Instance.NodeFromWorldPoint(requestee.transform.position));
			PathRequestManager.Instance.FinishedProcessingPath(waypoints, pathSuccess);
		}
		else if(requestee.GetType () == typeof(Enemy) && requestee.GetType() != typeof(StunEnemy))
			StartCoroutine (FindAlternatePath(startNode, targetNode, requestee));
		else
			PathRequestManager.Instance.FinishedProcessingPath(waypoints, pathSuccess);
	}

	IEnumerator FindAlternatePath(Node startNode, Node targetNode, Creature requestee)
	{
		List<Node> waypoints = new List<Node>();
		bool pathSuccess = false;

		if(startNode == targetNode){
			pathSuccess = false;
			PathRequestManager.Instance.FinishedProcessingPath(waypoints, pathSuccess);
			yield break;
		}

		//Try to find a path if both nodes are walkable
		if(startNode.walkable && targetNode.walkable)
		{
			Heap<Node> openNodes = new Heap<Node>(NodeManager.Instance.MaxSize);
			HashSet<Node> closedNodes = new HashSet<Node>();
			openNodes.Add(startNode);

			//Evaluate all nodes in the openNodes Heap and add evaluated nodes to the closed nodes group
			while(openNodes.Count > 0)
			{
				Node currentNode = openNodes.RemoveFirst();
				closedNodes.Add(currentNode);

				if(currentNode == targetNode)
				{
					pathSuccess = true;
					break;
				}

				//Check every neighbour of the node evaluated for better pathing options (lower fcost)
				foreach (Node neighbour in NodeManager.Instance.GetNeighbours(currentNode))
				{
					if(!neighbour.walkable || closedNodes.Contains(neighbour) || neighbour.containsTrap)
					{
						continue;
					}

					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
					if(newMovementCostToNeighbour < neighbour.gCost || !openNodes.Contains(neighbour))
					{
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

						if(!openNodes.Contains(neighbour))
							openNodes.Add(neighbour);					
						else
							openNodes.UpdateItem(neighbour);
					}
				}
			}
		}
		yield return null;
		if(pathSuccess) 
		{
			waypoints = RetracePath(startNode, targetNode);
			if (requestee.GetType () == typeof(Enemy) && requestee != null)
				waypoints = SimplifyPath (waypoints, NodeManager.Instance.NodeFromWorldPoint(requestee.transform.position));
		}
		PathRequestManager.Instance.FinishedProcessingPath(waypoints, pathSuccess);
	}

	List<Node> RetracePath(Node startNode, Node endNode)
	{
		List<Node> path = new List<Node>();
		Node currentNode = endNode;

		while (currentNode != startNode)
		{
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		path.Reverse();
		return path;
	}

	List<Node> SimplifyPath(List<Node> path, Node originNode)
	{
		int deltaX = path [0].gridX - originNode.gridX;
		int deltaY = path [0].gridY - originNode.gridY;

		List<Node> simplifiedPath = new List<Node>();

		for(int i = 1; i < path.Count; i++)
		{
			if((path [i].gridX - path [i-1].gridX) == deltaX && (path [i].gridY - path [i-1].gridY) == deltaY)
			{
				continue;
			}
			else
			{
				simplifiedPath.Add (path [i - 1]);
				deltaX = path [i].gridX - path [i - 1].gridX;
				deltaY = path [i].gridY - path [i - 1].gridY;
			}
		}
		simplifiedPath.Add(path [path.Count - 1]);
		return simplifiedPath;
	}

	int GetDistance(Node nodeA, Node nodeB)
	{
		int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

		if(distX > distY)
			return 14*distY + 10*(distX-distY);
		return 14*distX + 10*(distY-distX);
	}
}
