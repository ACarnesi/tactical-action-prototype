﻿using UnityEngine;
using System.Collections;

public class HookshotCommand : ICommand {

	private bool beganExecution, hooked, retracting;
	private float hookSpeed = 20;
	private int hookRange, distTraveled; 
	private Vector2 hookDir;
	private Vector3 lookDir, targetPos, hookPos; 
	private PlayerController creature;
	private Creature creatureHooked;
	private Node hookWaypoint, playerWaypoint, enemyWaypoint;
	private LineRenderer lR;

	//A move command must be constructed with a target position. 
	public HookshotCommand(PlayerController c, Vector3 tarPos)
	{
		beganExecution = false;
		retracting = false;
		hooked = false;
		targetPos = tarPos;
		creature = c;
		hookPos = c.transform.position;
		distTraveled = 0;
		hookRange = 7;

		lR = creature.GetComponent<LineRenderer> ();
		lR.SetColors (Color.black, Color.black);
		lR.SetWidth (.1f, .1f);

		Vector3 direction;
		if(creature.IsFollowingPath)
		{
			lR.SetPosition (0, c.CurrentWaypoint.worldPosition);
			lR.SetPosition (1, c.CurrentWaypoint.worldPosition);
			direction = (targetPos - creature.CurrentWaypoint.worldPosition).normalized;	
		}
		else
		{
			lR.SetPosition (0, c.CurrentNode.worldPosition);
			lR.SetPosition (1, c.CurrentNode.worldPosition);
			direction = (targetPos - creature.CurrentNode.worldPosition).normalized;
		}
		float theta = Vector2.Angle (Vector2.up, new Vector2 (direction.x, direction.z));

		if (theta >= 0f && theta < 22.5f)
		{
			lookDir = new Vector3(0,0,0);
			hookDir = new Vector2 (0, 1);
		}
		
		else if (theta >= 167.5f && theta <= 180f)
		{
			lookDir = new Vector3(0,180,0);
			hookDir = new Vector2 (0, -1);
		}

		if(direction.x > 0)
		{
			if (theta >= 22.5f && theta < 67.5f)
			{
				lookDir = new Vector3(0,45,0);
				hookDir = new Vector2 (1, 1);
			}
			
			else if (theta >= 67.5f && theta < 112.5f)
			{
				lookDir =  new Vector3(0,90,0);
				hookDir = new Vector2 (1, 0);
			}
			
			else if (theta >= 112.5f && theta < 167.5f)
			{
				lookDir =  new Vector3(0,135,0);
				hookDir = new Vector2 (1, -1);
			}
		}
		else
		{
			if (theta >= 22.5f && theta < 67.5f)
			{
				lookDir =  new Vector3(0,-45,0);
				hookDir = new Vector2 (-1, 1);
			}
			
			else if (theta >= 67.5f && theta < 112.5f)
			{
				lookDir =  new Vector3(0,-90,0);
				hookDir = new Vector2 (-1, 0);
			}
			
			else if (theta >= 112.5f && theta < 167.5f)
			{
				lookDir =  new Vector3(0,-135,0);
				hookDir = new Vector2 (-1, -1);
			}
		}
//
//		if(creature.IsFollowingPath)
//			hookWaypoint = NodeManager.Instance.NodeFromWorldPoint(creature.CurrentWaypoint.gridX + direction);
//		else
//			hookWaypoint = NodeManager.Instance.NodeFromWorldPoint(creature.transform.position + direction);
		
	}

	//Moves the ccter towards the target position. Returns true when within acceptable range of target position.
	public override bool ExecuteCommand()
	{
		//These are initialized once Execute is called the first time so that the player doesn't look in a different direction while another command is still being executed
		if(!beganExecution)
		{
			lR.enabled = true;
			creature.transform.eulerAngles = lookDir;
			beganExecution = true;
			creature.HookCooldown = 3.0f;
			hookWaypoint = NodeManager.Instance.grid [creature.CurrentNode.gridX + (int)hookDir.x, creature.CurrentNode.gridY + (int)hookDir.y];
		}

		float distToMove = hookSpeed * Time.deltaTime;
		if(!retracting)
		{
			//Prevents overshooting the waypoint
			if(distToMove < (Vector3.Magnitude(hookWaypoint.worldPosition - hookPos)))
			{
				Vector3 direction = (hookWaypoint.worldPosition - hookPos).normalized;
				hookPos += distToMove * direction;
				lR.SetPosition (1,hookPos);
				return false;
			}
			//Sets next waypoint or begins retraction
			else
			{
				hookPos = hookWaypoint.worldPosition;
				lR.SetPosition (1,hookPos);
				distTraveled++;
				if(distTraveled >= hookRange)
				{
					retracting = true;
					hookWaypoint = NodeManager.Instance.grid [hookWaypoint.gridX - (int)hookDir.x, hookWaypoint.gridY - (int)hookDir.y];
					return false;
				}

				if (hookWaypoint.walkable) 
				{
					if (hookWaypoint.CreatureOnNode != null) 
					{
						if (distTraveled <= 1) 
						{
							lR.enabled = false;
							return true;
						} 
						else 
						{
							hooked = true;
							retracting = true;
							creatureHooked = hookWaypoint.CreatureOnNode;
							creatureHooked.IsHooked = true;
							creatureHooked.IsDisabled = true;
							creatureHooked.CurrentNode.CreatureOnNode = null;
							creatureHooked.CurrentWaypoint.CreatureOnNode = null;
							creatureHooked.CurrentNode = hookWaypoint;
							creatureHooked.CurrentNode.CreatureOnNode = creatureHooked;
							creatureHooked.transform.position = creatureHooked.CurrentNode.worldPosition;
							enemyWaypoint = NodeManager.Instance.grid [creatureHooked.CurrentNode.gridX - (int)hookDir.x, creatureHooked.CurrentNode.gridY - (int)hookDir.y];
							//Safeguards against craeture becoming null via a trap and causing errors later in code (essentially initiates retraction with nothing hooked)
							if(enemyWaypoint.trapActive == true)
							{
								GameObject.Destroy (creatureHooked.gameObject);
								creatureHooked = null;
								hooked = false;
							}
							else
							{
								creatureHooked.CurrentWaypoint = enemyWaypoint;
								creatureHooked.CurrentWaypoint.CreatureOnNode = creatureHooked;
								playerWaypoint = NodeManager.Instance.grid [creature.CurrentNode.gridX + (int)hookDir.x, creature.CurrentNode.gridY + (int)hookDir.y];
								if(distTraveled > 2)
								{
									creature.CurrentWaypoint = playerWaypoint;
									creature.CurrentWaypoint.CreatureOnNode = creature;
								}
							}
							return false;
						}
					}
					else
					{
						hookWaypoint.walkable = false;
						hookWaypoint = NodeManager.Instance.grid[hookWaypoint.gridX + (int)hookDir.x, hookWaypoint.gridY + (int)hookDir.y];
					}
				}
				else
				{
					if (distTraveled <= 1) 
					{
						lR.enabled = false;
						return true;
					} 
					else 
					{
						hooked = true;
						retracting = true;
						creatureHooked = null;
						playerWaypoint = NodeManager.Instance.grid [creature.CurrentNode.gridX + (int)hookDir.x, creature.CurrentNode.gridY + (int)hookDir.y];
						playerWaypoint.CreatureOnNode = creature;
						creature.CurrentWaypoint = playerWaypoint;
						return false;
					}
				}
			}
		}
		else
		{
			if(hooked)
			{
				//A wall solid object is hooked. Pull the player to it.
				if(creatureHooked == null)
				{
					if(distToMove < (Vector3.Magnitude(playerWaypoint.worldPosition - creature.transform.position)))
					{
						Vector3 direction = (playerWaypoint.worldPosition - creature.transform.position).normalized;
						creature.transform.position += distToMove * direction;
						lR.SetPosition(0, creature.transform.position);
						return false;
					}
					
					else
					{
						creature.CurrentNode.CreatureOnNode = null;
						creature.CurrentNode = playerWaypoint;
						creature.transform.position = playerWaypoint.worldPosition;
						playerWaypoint.walkable = true;
						playerWaypoint = NodeManager.Instance.grid [creature.CurrentNode.gridX + (int)hookDir.x, creature.CurrentNode.gridY + (int)hookDir.y];
						if (playerWaypoint == hookWaypoint)
						{
							lR.enabled = false;
							return true;
						}
						else
						{
							creature.CurrentWaypoint = playerWaypoint;
							playerWaypoint.CreatureOnNode = creature;
						}
					}
				}
				//A creature is hooked, the player and creature are pulled towards each other (if the distance is odd, the creature moves the extra space)
				else
				{
					if (distTraveled <= 1)
					{
						creatureHooked.IsDisabled = false;
						creatureHooked.IsHooked = false;
						lR.enabled = false;
						return true;
					}
					else if(distTraveled > 2)
					{
						if (Vector3.Distance(creature.transform.position, playerWaypoint.worldPosition) >= .001 || Vector3.Distance(creatureHooked.transform.position, enemyWaypoint.worldPosition) >= .001)
						{
							//Player Movement
							if(distToMove < (Vector3.Magnitude(playerWaypoint.worldPosition - creature.transform.position)))
							{
								Vector3 direction = (playerWaypoint.worldPosition - creature.transform.position).normalized;
								creature.transform.position += distToMove * direction;
								lR.SetPosition(0, creature.transform.position);
							}

							else
							{
								creature.CurrentNode.CreatureOnNode = null;
								creature.CurrentNode = playerWaypoint;
								creature.transform.position = playerWaypoint.worldPosition;
								playerWaypoint.walkable = true;
//								playerWaypoint = NodeManager.Instance.grid [creature.CurrentNode.gridX + (int)hookDir.x, creature.CurrentNode.gridY + (int)hookDir.y];
							}

							//Enemy Movement
							if (distToMove < (Vector3.Magnitude (enemyWaypoint.worldPosition - creatureHooked.transform.position))) {
								Vector3 direction = (enemyWaypoint.worldPosition - creatureHooked.transform.position).normalized;
								creatureHooked.transform.position += distToMove * direction;
								lR.SetPosition (1, creatureHooked.transform.position);
							}
							else 
							{
								creatureHooked.CurrentNode.CreatureOnNode = null;
								creatureHooked.CurrentNode = enemyWaypoint;
								creatureHooked.transform.position = enemyWaypoint.worldPosition;
								enemyWaypoint.walkable = true;
							}
							return false;
						} 

						else
						{
							distTraveled -= 2;
							enemyWaypoint = NodeManager.Instance.grid [creatureHooked.CurrentNode.gridX - (int)hookDir.x, creatureHooked.CurrentNode.gridY - (int)hookDir.y];
							if(distTraveled > 2)
							{
								if(enemyWaypoint.trapActive == true)
								{
									GameObject.Destroy (creatureHooked.gameObject);
									creatureHooked = null;
									hooked = false;
								} 
								else 
								{
									creatureHooked.CurrentWaypoint = enemyWaypoint;
									creatureHooked.CurrentWaypoint.CreatureOnNode = creatureHooked;
									playerWaypoint = NodeManager.Instance.grid [creature.CurrentNode.gridX + (int)hookDir.x, creature.CurrentNode.gridY + (int)hookDir.y];
									creature.CurrentWaypoint = playerWaypoint;
									creature.CurrentWaypoint.CreatureOnNode = creature;
								}
							}
							if(distTraveled == 2)
							{
								creatureHooked.CurrentWaypoint = enemyWaypoint;
								creatureHooked.CurrentWaypoint.CreatureOnNode = creatureHooked;
							}
							return false;
						}
					}
					else
					{
						if(distToMove < (Vector3.Magnitude(enemyWaypoint.worldPosition - creatureHooked.transform.position)))
						{
							Vector3 direction = (enemyWaypoint.worldPosition - creatureHooked.transform.position).normalized;
							creatureHooked.transform.position += distToMove * direction;
							lR.SetPosition(1, creatureHooked.transform.position);
							return false;
						}
						else
						{
							creatureHooked.CurrentNode.CreatureOnNode = null;
							creatureHooked.CurrentNode = enemyWaypoint;
							creatureHooked.transform.position = enemyWaypoint.worldPosition;
							enemyWaypoint.walkable = true; 
							lR.enabled = false;
							creatureHooked.IsDisabled = false;
							creatureHooked.IsHooked = false;
							return true;
						}
					}
				}
			}
			//Nothing is hooked, retract the hookshot 
			else
			{
				if(distToMove < (Vector3.Magnitude(hookWaypoint.worldPosition - hookPos)))
				{
					Vector3 direction = (hookWaypoint.worldPosition - hookPos).normalized;
					hookPos += distToMove * direction;
					lR.SetPosition (1, hookPos);
					return false;
				}
				else
				{
					hookPos = hookWaypoint.worldPosition;
					lR.SetPosition (1, hookPos);
					if (hookWaypoint == creature.CurrentNode)
					{
						lR.enabled = false;
						return true;
					}
					else
					{
						hookWaypoint.walkable = true;
						hookWaypoint = NodeManager.Instance.grid [hookWaypoint.gridX - (int)hookDir.x, hookWaypoint.gridY - (int)hookDir.y];
						return false;
					}
				}
			}
		}
		return false;
	}

	public override void CancelCommand()
	{

	}
}
