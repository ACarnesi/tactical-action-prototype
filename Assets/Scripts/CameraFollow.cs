﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public float cameraDistance;

	PlayerController player;
	Vector3 lastPlayerPos;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		transform.position = player.transform.position + Vector3.up * cameraDistance;
		transform.LookAt(player.transform.position);
		lastPlayerPos = player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (!GUIManager.Instance.GamePaused)
        {
            Vector3 posDelta = player.transform.position - lastPlayerPos;
            transform.position = transform.position + posDelta;
            posDelta = Vector3.zero;
            lastPlayerPos = player.transform.position;
        }
	}
}
