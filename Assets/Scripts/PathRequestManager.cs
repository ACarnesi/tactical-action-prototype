﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PathRequestManager : MonoBehaviour {

	List<PathRequest> pathRequestList = new List<PathRequest>();
	PathRequest currentPathRequest;
	Pathfinding pathfinding;
	bool isProcessingPath;

	//Singleton pattern for PathRequestManager
	private static PathRequestManager instance;
	public static PathRequestManager Instance
	{
		get { 
			if (instance != null)
				return instance;
			else
				return instance = new GameObject ("PathRequestManager").AddComponent<PathRequestManager> ();
		}
	}

	void Awake()
	{
		pathfinding = GameObject.FindGameObjectWithTag("NodeManager").GetComponent<Pathfinding>();
	}

	//Creates a new request to find a path and adds it to the queue of requests
	public void RequestPath(Creature requestee, Node pathStart, Node pathEnd, Action<List<Node>, bool> callback) 
	{
		PathRequest newRequest = new PathRequest (requestee, pathStart, pathEnd, callback);
		foreach(PathRequest p in pathRequestList)
		{
			if(p.requestee == requestee)
			{
				instance.pathRequestList[pathRequestList.IndexOf(p)] = newRequest;
				instance.TryProcessNext();
				return;
			}
		}
		instance.pathRequestList.Add(newRequest);
		instance.TryProcessNext();
	}

	//Processes the next path if it is not currently processing one
	void TryProcessNext()
	{
		if (!isProcessingPath && pathRequestList.Count > 0) 
		{
			currentPathRequest = pathRequestList[0];
			pathRequestList.Remove(currentPathRequest);
			isProcessingPath = true;
			pathfinding.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd, currentPathRequest.requestee);
		}
	}

	//Runs the callback method when the path has failed or been successfully found, and then the next path is processed
	public void FinishedProcessingPath(List<Node> path, bool success)
	{
		if (currentPathRequest.requestee != null)
			currentPathRequest.callback(path, success);
		isProcessingPath = false;
		TryProcessNext();
	}
		
	struct PathRequest 
	{
		public Creature requestee; 
		public Node pathStart, pathEnd;
		public Action<List<Node>, bool> callback;

		public PathRequest(Creature _requestee, Node _start, Node _end, Action<List<Node>, bool> _callback)
		{
			requestee = _requestee;
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
		}
	}
}
