﻿using UnityEngine;
using System.Collections;

public class DashCommand : ICommand {

	private PlayerController creature;
	private Vector3 targetPos;
	private Vector3 lookDir;
	private Vector2 dashDir;
	private Node nextNode;
	private bool isBetweenNodes, beganExecution;
	private int power;

	//A move command must be constructed with a target position. 
	public DashCommand(PlayerController c, Vector3 tarPos)
	{
		targetPos = tarPos;
		creature = c;
		power = 5;
		isBetweenNodes = false;
		beganExecution = false;
		Vector3 direction;
		if(creature.IsFollowingPath)
			direction = (targetPos - creature.CurrentWaypoint.worldPosition).normalized;
		else
			direction = (targetPos - creature.transform.position).normalized;		

		float theta = Vector2.Angle (Vector2.right, new Vector2 (direction.x, direction.z));

		if (theta >= 0f && theta < 22.5f)
			dashDir = Vector2.right;
		else if (theta >= 167.5f && theta <= 180f)
			dashDir = Vector2.left;
		
		if(direction.z > 0)
		{
			if (theta >= 22.5f && theta < 67.5f)
				dashDir = new Vector2 (1, 1);
			else if (theta >= 67.5f && theta < 112.5f)
				dashDir = new Vector2 (0, 1);
			else if (theta >= 112.5f && theta < 167.5f)
				dashDir = new Vector2 (-1, 1);
		}
		else
			if (theta >= 22.5f && theta < 67.5f)
				dashDir = new Vector2 (1, -1);
			else if (theta >= 67.5f && theta < 112.5f)
				dashDir = new Vector2 (0, -1);
			else if (theta >= 112.5f && theta < 167.5f)
				dashDir = new Vector2 (-1,-1);

		theta = Vector2.Angle (Vector2.up, new Vector2 (direction.x, direction.z));
		if (theta >= 0f && theta < 22.5f)
			lookDir = new Vector3(0,0,0);
		else if (theta >= 167.5f && theta <= 180f)
			lookDir = new Vector3(0,180,0);
		if(direction.x > 0)
		{
			if (theta >= 22.5f && theta < 67.5f)
				lookDir = new Vector3(0,45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,135,0);
		}
		else
			if (theta >= 22.5f && theta < 67.5f)
				lookDir =  new Vector3(0,-45,0);
			else if (theta >= 67.5f && theta < 112.5f)
				lookDir =  new Vector3(0,-90,0);
			else if (theta >= 112.5f && theta < 167.5f)
				lookDir =  new Vector3(0,-135,0);

		if(creature.IsFollowingPath)
			nextNode = NodeManager.Instance.grid [(creature.CurrentWaypoint.gridX + (int)dashDir.x), (creature.CurrentWaypoint.gridY + (int)dashDir.y)];
		else
			nextNode = NodeManager.Instance.grid [(creature.CurrentNode.gridX + (int)dashDir.x), (creature.CurrentNode.gridY + (int)dashDir.y)];
	}

	//Moves the ccter towards the target position. Returns true when within acceptable range of target position.
	public override bool ExecuteCommand()
	{
		if(!beganExecution)
		{
			creature.transform.eulerAngles = lookDir;
			creature.DashCooldown = 5.0f;
			creature.DashTrail = true;
			beganExecution = true;
		}
		if (!isBetweenNodes) {
			if (!nextNode.walkable)
			{
				creature.DashTrail = false;
				return true;
			}
			else if (nextNode.CreatureOnNode != null) {
				nextNode.CreatureOnNode.ApplyKnockback (dashDir, power);
				creature.DashTrail = false;
				return true;
			}
			else {
				nextNode.CreatureOnNode = creature;
				isBetweenNodes = true;
				return false;
			}
		} else {
			float distMoved = creature.speed * 4 * Time.deltaTime;
			if (distMoved < (Vector3.Magnitude (nextNode.worldPosition - creature.transform.position))) {
				Vector3 direction = (nextNode.worldPosition - creature.transform.position).normalized;
				creature.transform.position += distMoved * direction;
				return false;
			} else {
				creature.transform.position = nextNode.worldPosition;
				creature.CurrentNode.CreatureOnNode = null;
				creature.CurrentNode = nextNode;
				nextNode = NodeManager.Instance.grid [(creature.CurrentNode.gridX + (int)dashDir.x), (creature.CurrentNode.gridY + (int)dashDir.y)];
				power--;
				isBetweenNodes = false;
				if (power <= 0)
				{
					creature.DashTrail = false;
					return true;
				}
				else
					return false;
			}
		}
	}

	public override void CancelCommand()
	{
		
	}
}
