﻿using UnityEngine;
using System.Collections;

public class SpikeTrap : MonoBehaviour {

	public float alternateTime;
	public bool trapActive, alternating;
	public SpriteRenderer placeholder;

	float timeUntilAlternate;
	float flashSpeed = .25f;
	Node currentNode;

	void Start()
	{
		placeholder = GetComponent<SpriteRenderer> ();
		currentNode = NodeManager.Instance.NodeFromWorldPoint (transform.position);
		currentNode.trapActive = trapActive;
		currentNode.containsTrap = true;
		timeUntilAlternate = alternateTime;
		if (trapActive)
			placeholder.color = Color.red;
		else
			placeholder.color = Color.grey;

		StartCoroutine ("TrapUpdate");
	}

	IEnumerator TrapUpdate()
	{
		while(true)
		{
			
			if (timeUntilAlternate <= 0)
			{
				trapActive = !trapActive;
				currentNode.trapActive = trapActive;
				timeUntilAlternate = alternateTime;
				alternating = false;
				
				if (trapActive)
					placeholder.color = Color.red;
				else
					placeholder.color = Color.grey;
			}
			else
			{
				timeUntilAlternate -= Time.deltaTime;
				if(timeUntilAlternate <= alternateTime/5)
				{
					flashSpeed -= Time.deltaTime;
					if(flashSpeed <= 0)
					{
						alternating = true;
						if(trapActive)
							placeholder.color = (placeholder.color == Color.red) ? Color.yellow : Color.red;
						else
							placeholder.color = (placeholder.color == Color.grey) ? Color.yellow : Color.grey;
						flashSpeed = .25f;
					}
				}
			}
			
			if (currentNode.creature != null && trapActive == true)
            {
                if (currentNode.creature.GetType().IsSubclassOf(typeof(Enemy)))
                {
                    GUIManager.Instance.UpdateEnemyCount();
                    Destroy(currentNode.creature.gameObject);
                }
                else
                    GUIManager.Instance.GameOver();
            }
			yield return null;
		}
	}
}
