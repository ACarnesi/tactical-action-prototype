﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PlayerController : Creature {

	public bool debugging;
	public List<Node> path = new List<Node>();
	public LayerMask enemyMask;

	int targetIndex;
	float timeDisabled;
	float dashCooldown = 0;
	float hookCooldown = 0;
	float playerHealth;
	float requestBuffer;
	TrailRenderer dashTrail;

	public float DashCooldown{
		get{return dashCooldown;}
		set
		{
			dashCooldown = value;
			GUIManager.Instance.DashCooldown = value;
		}
	}

	public float HookCooldown{
		get{return hookCooldown;}
		set
		{
			hookCooldown = value;
			GUIManager.Instance.HookshotCooldown = value;
		}
	}

	public float TeleCooldown{
		get{return teleCooldown;}
		set
		{
			teleCooldown = value;
			GUIManager.Instance.TeleportCooldown = value;
		}
	}

	public float PlayerHealth{
		get{return playerHealth;}
		set
		{
			playerHealth = value;
			GUIManager.Instance.HealthSlider = value;
		}
	}

	public bool DashTrail{
		get{return dashTrail.enabled;}
		set{dashTrail.enabled = value;}
	}

	new void Start()
	{
		base.Start ();
		playerHealth = 100;
		dashTrail = GetComponent<TrailRenderer> ();
		dashTrail.enabled = false;
	}

	void Update()
	{
        if (!GUIManager.Instance.GamePaused)
        {
            if (!IsDisabled)
		    {
			    #if !UNITY_EDITOR
			    if (Input.GetKey (KeyCode.Escape))
				    Application.Quit();
			    #endif
			    if (requestBuffer <= 0) 
			    {
				    requestBuffer = .1f;
				    if(Input.GetAxis("Dash") > 0)
				    {
					    if(dashCooldown <= 0)
					    {
						    Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
						    RaycastHit hitInfo;
						    if (Physics.Raycast (ray, out hitInfo, 100)) 
						    {
							    if (currentCommand == null)
								    currentCommand = new DashCommand (this, hitInfo.point);
							    else if(currentCommand.GetType() != typeof(DashCommand))
								    nextCommand = new DashCommand (this, hitInfo.point);
						    }
					    }
				    }
				
				    if(Input.GetAxis("Teleport") > 0)
				    {
                        if (teleCooldown <= 0)
					    {
						    Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
						    RaycastHit hitInfo;
						    if (Physics.Raycast (ray, out hitInfo, 100)) 
						    {
    //							if(Physics.Raycast(transform.position, hitInfo.point, out hitInfo, 100, enemyMask))
    //							{
								    if (NodeManager.Instance.NodeFromWorldPoint (hitInfo.point).walkable) {
									    if (currentCommand == null)
										    currentCommand = new TeleportCommand (this, hitInfo.point);
									    else
										    nextCommand = new TeleportCommand (this, hitInfo.point);
									
								    }
    //							}
						    }
					    }
				    }
				
				    else if (Input.GetAxis ("Interact") > 0) 
				    {
                        Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					    RaycastHit hitInfo;
					    if (Physics.Raycast (ray, out hitInfo, 100)) 
					    {
						    //If character hodling position, attack in relative direction
						    if (Input.GetAxis ("Shift") > 0) 
						    {
							
						    }
						    //Move to location/ Interact with object/ Attack enemy in range
						    else 
						    {
							    if (!isFollowingPath)
								    RequestPath(transform.position, hitInfo.point);
							    else
								    RequestPath(transform.position, hitInfo.point);
						    }
					    }
				    }
								    else if(Input.GetMouseButton(1)) 
				    {
					    if(hookCooldown <= 0)
					    {
						    Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
						    RaycastHit hitInfo;
						    if (Physics.Raycast (ray, out hitInfo, 100)) 
						    {
							    if (NodeManager.Instance.NodeFromWorldPoint (hitInfo.point).walkable) {
								    if (currentCommand == null)
									    currentCommand = new HookshotCommand (this, hitInfo.point);
								    else
									    nextCommand = new HookshotCommand (this, hitInfo.point);
							    }
						    }
					    }
    //					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
    //					RaycastHit hitInfo;
    //					if (Physics.Raycast (ray, out hitInfo, 100)) 
    //					{
    //						Debug.Log ("Clicked Point: " + hitInfo.point.ToString ());
    //						Node n = NodeManager.Instance.NodeFromWorldPoint (hitInfo.point);
    //						Debug.Log ("Clicked Node: " + n.gridX.ToString () + "," + n.gridY.ToString ());
    //						if(n.CreatureOnNode != null)
    //							Debug.Log ("Creature At this Node: " + n.CreatureOnNode.ToString ());
    //					}
				    }
			    } else
				    requestBuffer -= Time.deltaTime;
			
			    if(currentCommand != null && currentCommand.GetType() != typeof(MoveCommand))
			    {
				    if (currentCommand.ExecuteCommand())
				    {
					    currentCommand = nextCommand;
					    nextCommand = null;
				    }
			    }
			
			    if (DashCooldown > 0)
				    DashCooldown -= Time.deltaTime;
			    if (TeleCooldown > 0)
				    TeleCooldown -= Time.deltaTime;
			    if (HookCooldown > 0)
				    HookCooldown -= Time.deltaTime;
		    }
		    else
		    {
			    if (timeDisabled <= 0) {
				    IsDisabled = false;
			    } else
				    timeDisabled -= Time.deltaTime;
		    }
        }
	}

	protected override void RequestPath(Vector3 startPos, Vector3 targetPos)
	{
		Node targetNode = NodeManager.Instance.NodeFromWorldPoint (targetPos);
		if (currentCommand != null)
			nextCommand = new PathRequestCommand (this, targetNode);
		else
			currentCommand = new PathRequestCommand (this, targetNode);
	}
	
	//If a path is returned and successful, start following the path
	public override void OnPathFound(List<Node> newPath, bool pathSuccessful){
		if(!isDisabled)
		{
			if(pathSuccessful)
			{
				//If the player is already following a path, continue its movement to its currentWaypoint and append the new path to the end of that. 
				if(!isFollowingPath)
				{
					path = newPath;
					StartCoroutine("PursueTarget");
				}
				else
				{
					newPath.Insert(0, path[targetIndex]);
					targetIndex = 0;
					path = newPath;
				}
			}
		}
	}

	IEnumerator PursueTarget()
	{
        if(!GUIManager.Instance.GamePaused)
        {
		    currentWaypoint = path [targetIndex];
		    if (path[targetIndex].CreatureOnNode == null) {
			    //If a command is currently being executed, wait until it has finished. Else set current command to a new MoveCommand and continue
			    if (currentCommand != null) {
				    nextCommand = new MoveCommand (this, currentWaypoint.worldPosition);
				    while (currentCommand.GetType() != typeof(MoveCommand)) {
					    yield return null;
					    //If the NextCommand is overridden by another command, break out of PursueTarget
					    if (nextCommand != null && nextCommand.GetType() != typeof(MoveCommand))
						    yield break;
				    }
				    //If the current waypoint is no longer empty 
				    if(currentWaypoint.CreatureOnNode != null)
				    {
					    //Attack if it is your target, else Request a new path and break out of PursueTarget. 
					    if(currentWaypoint.CreatureOnNode == targetCreature)
					    {
						    Attack (path [targetIndex].CreatureOnNode);
    //						currentCommand = new AttackCommand (currentWaypoint);
						    isFollowingPath = false;
						    targetIndex = 0;
						    yield break;
					    }
					    else
					    {
						    currentCommand = null;
						    RequestPath(transform.position ,targetCreature.transform.position);
						    isFollowingPath = false;
						    targetIndex = 0;
						    yield break;
					    }
				    }
			    } else
				    currentCommand = new MoveCommand (this, currentWaypoint.worldPosition);

			    path [targetIndex].CreatureOnNode = this;
			    isFollowingPath = true;
		    }
		    else if(currentWaypoint.CreatureOnNode == targetCreature)
		    {
			    Attack (currentWaypoint.CreatureOnNode);
			    isFollowingPath = false;
			    targetIndex = 0;
			    yield break;
		    }
		    else
			    yield break;

		    while(true)
		    {
                if (!GUIManager.Instance.GamePaused)
                {
                    if (!IsDisabled)
                    {
                        //If you have reached the current waypoint, check if you have gone through all the waypoints in the path. If yes, stop followingPath
                        if (currentCommand.ExecuteCommand())
                        {
                            if (nextCommand != null)
                            {
                                currentCommand = nextCommand;
                                nextCommand = null;
                                currentNode.CreatureOnNode = null;
                                currentNode = currentWaypoint;
                                isFollowingPath = false;
                                targetIndex = 0;
                                yield break;
                            }
                            else
                                currentCommand = null;

                            if (targetIndex > 0)
                                path[targetIndex - 1].CreatureOnNode = null;
                            else
                                CurrentNode.CreatureOnNode = null;

                            CurrentNode = path[targetIndex];
                            targetIndex++;


                            if (targetIndex >= path.Count)
                            {
                                isFollowingPath = false;
                                targetIndex = 0;
                                yield break;
                            }

                            //If this is the last waypoint, and it contains a creature, interact with them and stop followingPath
                            if (targetIndex == path.Count - 1)
                            {
                                if (path[targetIndex].CreatureOnNode != null)
                                {
                                    Attack(path[targetIndex].CreatureOnNode);
                                    isFollowingPath = false;
                                    targetIndex = 0;
                                    yield break;
                                }
                            }

                            //Check next waypoint for target/obstruction. Attack/Repath if true 
                            if (path[targetIndex].CreatureOnNode != null)
                            {
                                if (path[targetIndex].CreatureOnNode == targetCreature)
                                {
                                    Attack(path[targetIndex].CreatureOnNode);
                                    isFollowingPath = false;
                                    targetIndex = 0;
                                    yield break;
                                }
                                else
                                {
                                    RequestPath(transform.position, path[path.Count - 1].worldPosition);
                                    isFollowingPath = false;
                                    targetIndex = 0;
                                    yield break;
                                }
                            }

                            //If the target has moved, request new path to correct
                            if (targetCreature != null)
                            {
                                if (NodeManager.Instance.NodeFromWorldPoint(targetCreature.transform.position) != path[path.Count - 1])
                                {
                                    RequestPath(transform.position, targetCreature.transform.position);
                                    isFollowingPath = false;
                                    targetIndex = 0;
                                    yield break;
                                }
                            }
                            //Continue to the next waypoint 
                            currentWaypoint = path[targetIndex];
                            path[targetIndex].CreatureOnNode = this;
                            currentCommand = new MoveCommand(this, currentWaypoint.worldPosition);
                        }
                        //			transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.worldPosition, speed * Time.deltaTime);
                        yield return null;
                    }
                    else
                        yield return null;
                }
                else
                    yield return null;
		    }
        }
	}

	public void TakeDamage(float dmgAmount)
	{
		PlayerHealth -= dmgAmount;
		if(PlayerHealth <= 0)
		{
            GUIManager.Instance.GameOver();
		}
	}

	public override void Attack(Creature c)
	{
		//Kicks and knocks back the enemy in a direction relative to the player
		Vector2 nodeDelta;
		if (c.CurrentNode.gridX - currentNode.gridX > 0)
			nodeDelta.x = 1;
		else if (c.CurrentNode.gridX - currentNode.gridX < 0)
			nodeDelta.x = -1;
		else
			nodeDelta.x = 0;
		
		if (c.CurrentNode.gridY - currentNode.gridY > 0)
			nodeDelta.y = 1;
		else if (c.CurrentNode.gridY - currentNode.gridY < 0)
			nodeDelta.y = -1;
		else
			nodeDelta.y = 0;
		c.ApplyKnockback (nodeDelta, 3);
	}
		
	public override void Disable(float duration)
	{
		timeDisabled = duration;
		isDisabled = true;
	}

	void OnDrawGizmos()
	{
		if(debugging){
			if (path != null)
			{
				for (int i = targetIndex; i < path.Count; i++)
				{
					Gizmos.color = Color.black;
					Gizmos.DrawCube(path[i].worldPosition, Vector3.one);
					
					if (i == targetIndex) 
						Gizmos.DrawLine(transform.position, path[i].worldPosition);
					else 
						Gizmos.DrawLine(path[i-1].worldPosition, path[i].worldPosition);
				}
			}
		}
	}
}
